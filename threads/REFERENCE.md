thread
===========

This directory saves some example source code files for multi-threading programs in Python.

Useful Links
=============

* http://www.tutorialspoint.com/python/python_multithreading.htm