#!/usr/bin/python

import os
from pwd import getpwnam

HOME_PATH = os.path.expanduser("~")
OS_USER = os.getenv("SUDO_USER")
if OS_USER == None or OS_USER == "":
	# the user isn't running sudo
	OS_USER = os.getenv("USER")
else:
	# when in SUDO, fix the HOME_PATH
	# may not be necessary on most OSes
	HOME_PATH = os.path.split(HOME_PATH)[0] + "/" + OS_USER

os.mkdir("test", 0700)
os.chown("test", getpwnam(OS_USER).pw_uid, getpwnam(OS_USER).pw_gid)