#!/usr/bin/python

def YesNoQuery(question, answer = "y"):
	"""Print the question, and return True if the user input is the expected one."""
	valid = {"y": True, "ye": True, "yes": True,
			 "n": False, "no": False}
	if answer == "y":
		prompt = " [Y/n] "
	elif answer == "n":
		prompt = " [y/N] "
	else:
		prompt = " [y/n] "
	
	print question + prompt
	while True:
		response = raw_input().lower()
		if answer is not None and response == "":
			return answer
		elif response in valid.keys():
			return valid[response]
		else:
			print "Please respond with 'y' (yes) or 'n' (no).\n"

"""This is an example usage"""
if YesNoQuery("Do you love me?"):
	print "I love you,too"
	if YesNoQuery("Will you leave me?", "n"):
		print "I won't leave you, either."
	else:
		print "Don't leave me, please!"
else:
	print "..."