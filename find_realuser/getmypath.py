#!/usr/bin/python

import os

LOCAL_USER = os.getenv("SUDO_USER")
if LOCAL_USER == None or LOCAL_USER == "":
	LOCAL_USER = os.getenv("USER")

HOME_PATH = os.path.expanduser("~" + LOCAL_USER)

print "The actual user is " + LOCAL_USER
print "The actual home dir is " + HOME_PATH
